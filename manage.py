from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from app import app, db

manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()

# help
# docker-compose run web python manage.py db init
# docker-compose run web python manage.py db migrate
# docker-compose run web python manage.py db upgrade
