class PRecipeForm extends HTMLElement
{
    constructor()
    {
        super();
        this._root = this.attachShadow({mode: 'open'});
    }

    connectedCallback() {
        this._root.innerHTML = `
            <style>
            h1 {
                color: #fff;
                text-align: center;
            }
            form {
            	font-size: 16px;
            	background: #495C70;
            	padding: 30px 30px 15px 30px;
            	border: 5px solid #53687E;
            }
            form input[type=submit],
            form input[type=button],
            form input[type=text],
            form textarea,
            form div
            {
            	color: #fff;
            }
            form div {
            	display: flex;
                align-items: center;
            	margin-bottom: 10px;
            }
            form div > label{
            	width: 20%;
                font-size: 1.5em
            }
            form input[type=text]
            {
                font-size: 1.5em;
            	background: transparent;
            	border: none;
            	border-bottom: 1px dashed #83A4C5;
            	width: 100%;
            	outline: none;
            }
            form textarea{
            	background: transparent;
            	outline: none;
            	border: none;
            	border-bottom: 1px dashed #83A4C5;
            	width: 100%;
            	overflow: hidden;
            	resize:none;
            	height:20px;
            }
            form textarea:focus,
            form input[type=text]:focus
            {
            	border-bottom: 1px dashed #D9FFA9;
            }
            form input[type=submit],
            form input[type=button] {
            	background: #576E86;
            	border: none;
            	padding: 8px 10px 8px 10px;
            	color: #A8BACE;
                font-size: 1.5em;
                margin-top: 20px;
            }
            form input[type=submit]:hover,
            form input[type=button]:hover{
                background: #394D61;
            }
            </style>
            <form method="POST">
                <h1>Ajouter une nouvelle recette</h1>
                <div class="row">
                    <label>Titre</label><input type="text" name="name" required="true" placeholder="Titre de la recette"/>
                </div>
                <div class="row">
                    <label>Image url</label><input type="text" name="url" required="true" placeholder="Image de la recette"/>
                </div>
                <div>
                    <input type="submit" value="Enregistrer" />
                </div>
            </form>
        `;
    }
}
window.customElements.define('p-recipe-form', PRecipeForm);
