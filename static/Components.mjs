class Components extends HTMLElement {
    constructor() {
        super();

        this._root = this.attachShadow({mode: 'open'});
    }

    connectedCallback() {
        this._json = JSON.parse(this.getAttribute('json'))
        this._root.innerHTML = `
            <style>
                p {
                    color: red;
                }
                div {
                    background-color: grey;
                    padding: 5px;
                    margin: 5px;
                }
            </style>
        `;

        this._json.forEach((json) => {
            let div = document.createElement('div');
            div.innerHTML = `
                <p>Info Composant</p>
                <label>Nom = ${json.nom}</label>
                <label>Type = ${json.type}</label>
                <p>Configuration Composant</p>
                <label>${json.configuration.ip}:${json.configuration.port}</label>
            `;
            this._root.appendChild(div);
        });
    }
}
window.customElements.define('p-comp', Components);
