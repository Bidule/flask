export class PForm extends HTMLElement {
    constructor() {
        super();

        this._root = this.attachShadow({mode: 'open'});

        this._socket = io.connect('http://' + document.domain + ':' + location.port);


    }

    connectedCallback() {
        this._root.innerHTML = `
            <style>
                #click {
                    background-color: red;
                }
            </style>
            <div id="click">OOPS!!! IT WORK!!</div>`;

        this._click = this._root.querySelector('#click');

        this._click.addEventListener('click', () => {
            this._socket.emit('my event', {
                name : 'batatch',
                url : 'batatch.jpg'
            });
        });

        this._socket.on('my response', (msg) => {
            console.log(msg);
        });
    }
}
window.customElements.define('p-form', PForm);
