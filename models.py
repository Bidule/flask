from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

# models
class Product(db.Model):
    __tablename__ = 'product'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False)
    url = db.Column(db.String(128), nullable=False)

    def __init__(self, name, url):
        self.name = name
        self.url = url

    def __repr__(self):
        return '[Product] {name}'.format(name=self.name)

class Recipe(db.Model):
    __tablename__ = 'recipe'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False)
    url = db.Column(db.String(128), nullable=False)

    def __init__(self, name, url):
        self.name = name
        self.url = url

    def __repr__(self):
        return '[Recipe] {name}'.format(name=self.name)

class Preparation(db.Model):
    __tablename__ = 'preparation'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe.id'), nullable=False)
    step = db.Column(db.Integer)
    action = db.Column(db.String(512), nullable=False)

    def __init__(self, recipeId, step, action):
        self.recipe_id = recipeId
        self.step = step
        self.action = action

    def __repr__(self):
        return '[Preparation] Step {step} - {action}'.format(step=self.step, action=self.action)

class Ingredient(db.Model):
    __tablename__ = 'ingredient'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe.id'), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)
    quantity = db.Column(db.Integer)
    unit = db.Column(db.String(32), nullable=False)

    def __init__(self, recipeId, productId, quantity, unit):
        self.recipe_id = recipeId
        self.product_id = productId
        self.quantity = quantity
        self.unit = unit

    def __repr__(self):
        return '[Ingredient] {quantity} {unit}'.format(quantity=self.quantity, unit=self.unit)
