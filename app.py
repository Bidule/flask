from flask import Flask, render_template, request, redirect, url_for, json
from models import db, Product, Recipe
from flask_socketio import SocketIO
from cache import components

app = Flask(__name__)

# psql
POSTGRES = {
    'user': 'cuistot',
    'pw': 'miam',
    'db': 'cuisine',
    'host': 'db',
    'port': '5432',
}
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://{user}:{pw}@{host}:{port}/{db}'.format(**POSTGRES)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:postgres@db/cuisine'
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'ieguiaege4a864*:aeoaj;;e::#'
db.init_app(app)

# routes
@app.route('/oops')
def oops():
    return render_template('oops.html', components=json.dumps(components))

@app.route('/')
def welcome():
    return render_template('index.html')

@app.route('/products', methods=['GET', 'POST'])
def rProducts():
    if request.method == "POST":
        try:
            product = Product(name=request.form.get('name'), url=request.form.get('url'))
            db.session.add(product)
            db.session.commit()
        except Exception as e:
            print("Failed to add product")
            print(e)
    productsList = Product.query.order_by(Product.name).all()
    return render_template('products.html', productsList=productsList)

@app.route('/products/<name>', methods=["GET"])
def rProduct(name):
    product = Product.query.filter_by(name=name).first()
    return render_template('product.html', product=product)

@app.route("/update", methods=["POST"])
def rUpdate():
    newname = request.form.get("newname")
    oldname = request.form.get("oldname")
    product = Product.query.filter_by(name=oldname).first()
    product.name = newname
    db.session.commit()
    return redirect("products")

@app.route("/delete", methods=["POST"])
def rDelete():
    name = request.form.get("name")
    product = Product.query.filter_by(name=name).first()
    db.session.delete(product)
    db.session.commit()
    return redirect("products")

@app.route("/recipes", methods=["GET"])
def rRecipe():
    recipesList = Recipe.query.order_by(Recipe.name).all()
    return render_template('recipes.html', recipesList=recipesList)

@app.route("/recipes/new", methods=["GET", "POST"])
def rRecipesNew():
    if request.method == "POST":
        try:
            recipe = Recipe(name=request.form.get('name'), url=request.form.get('url'))
            db.session.add(recipe)
            db.session.commit()
        except Exception as e:
            print("Failed to add recipe")
            print(e)
        return redirect("recipes")
    productsList = Product.query.order_by(Product.name).all()
    return render_template('recipesNew.html', productsList=productsList)

@app.route("/recipes/delete", methods=["GET", "POST"])
def rRecipesDelete():
    if request.method == "POST":
        name = request.form.get("recipes")
        recipe = Recipe.query.filter_by(name=name).first()
        db.session.delete(recipe)
        db.session.commit()
        return redirect("recipes")
    recipesList = Recipe.query.order_by(Recipe.name).all()
    return render_template('recipesDelete.html', recipesList=recipesList)

# socket IO
socketio = SocketIO(app)

def messageReceived(methods=['GET', 'POST']):
    print('message was received!!!')

@socketio.on('my event')
def handle_my_custom_event(json, methods=['GET', 'POST']):
    socketio.emit('my response', {'response': 'my response'})

if __name__ == '__main__':
    socketio.run(app, debug=True, host='0.0.0.0')
    #app.run('0.0.0.0')
