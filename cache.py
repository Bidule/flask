components = [
    {
        "type": "reseau",
        "configuration": {
            "ip": "1.1.1.1",
            "port": "55"
        },
        "nom" : "Bidule"
    },
    {
        "type": "reseau",
        "configuration": {
            "ip": "192.168.5.22",
            "port": "7778"
        },
        "nom" : "Machin"
    },
    {
        "type": "codec",
        "configuration": {
            "ip": "192.168.5.22",
            "port": "7778"
        },
        "nom" : "NMEA Decoder"
    }
];
